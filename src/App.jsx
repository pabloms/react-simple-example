import './App.scss';
import Header from './components/Header';
import Jobs from './components/Jobs';
import Education from './components/Education';
import NewJobForm from './components/NewJobForm';
import React from 'react';
import { useState } from 'react';

const curriculum = {
  firstName: "Pablo",
  lastName: "Mendez Suarez",
  image: "https://media-exp1.licdn.com/dms/image/C4E03AQEQK6hkXnt7uw/profile-displayphoto-shrink_400_400/0/1607971580045?e=1632355200&v=beta&t=hlv6thF8H0DTAMPrcFRLn6xwO0q11KxGOzCgcxZEdgU",
  profession: "FullStack Developer",
  education: [
    {
      title: "Bachillerato en Administración de Empresas",
      schoolName: "Colegio F.A.S.T.A.",
      description: "Orientado a econommía e administración de empresas, desde el año 2009 hasta el año 2014, en Argentina."
    },
    {
      title: "Ingeniería en Sistemas de Información",
      schoolName: "Universidad Tecnologica Nacional (U.T.N.)",
      description: "Cursada y aprobada hasta el 3er año, desde el año 2015 hasta el año 2019, en Argentina."
    },
    {
      title: "FullStack Developer",
      schoolName: "Upgrade-Hub",
      description: "Bootcamp de FullStack en curso hasta Junio de 2021."
    }
  ],
  jobs : [
    {
      workstation: "Cadete/Empleado",
      companyName: "Farmacia Suarez",
      description: "Trabajo relacionado a la reposición de stock de la mercadería, atención en caja y envíos a domicilio de productos farmaceuticos."
    },    
  ]
}

// class App extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//         cv: cv,
//         showForm : false,
//     }
// };

//     addJob = (job) => {
//       this.setState({
//         cv : {
//           ...this.state.cv,
//           jobs :  [...this.state.cv.jobs, job]
//         }
//       })
//   }

//   render () {
//     return (
//     <div className="Box">
//       <div className="App">
//         <Header
//           fullName = {`${cv.firstName} ${cv.lastName}`}
//           profession = {cv.profession}
//           image = {cv.image}
//         />
//         <div className="info">
//           <div className="info__section">
//             <h1 className="info__title">Formación Académica</h1>
//             <Education name="Education" elements={cv.education}/>
//           </div>
//           <div className="info__section">
//             <h1 className="info__title">Experiencia/Trabajos</h1>
//             <Jobs name="Jobs" elements={this.state.cv.jobs}/>
//             <div>
//               <button className="info__button" onClick={() => this.setState({showForm: !this.state.showForm})}>Agregar Experiencia</button>
//               {this.state.showForm && <NewJobForm addJob={this.addJob}/>}
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//     )
//   }
// }

const App = () => {

  const [showForm, setShowForm] = useState(false);
  const [cv, setCv] = useState(curriculum);

  const addJob = (job) => {
    setCv({
        ...cv,
        jobs: [...cv.jobs, job]
    })
  }

  return (
    <div className="Box">
        <div className="App">
          <Header
            fullName = {`${cv.firstName} ${cv.lastName}`}
            profession = {cv.profession}
            image = {cv.image}
          />
          <div className="info">
            <div className="info__section">
              <h1 className="info__title">Formación Académica</h1>
              <Education name="Education" elements={cv.education}/>
            </div>
            <div className="info__section">
              <h1 className="info__title">Experiencia/Trabajos</h1>
              <Jobs name="Jobs" elements={cv.jobs}/>
              <div>
                <button className="info__button" onClick={() => setShowForm(!showForm)}>Agregar Experiencia</button>
                {showForm && <NewJobForm addJob={addJob}/>}
              </div>
            </div>
          </div>
        </div>
    </div>
  )
}

export default App;
