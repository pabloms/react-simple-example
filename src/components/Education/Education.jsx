import React from 'react';
import './Education.scss';
import EducationItem from '../EducationItem';

// class Education extends React.Component {
//     render() {
//         return (
//             <div className="education">
//                 <div className="education__list">
//                     {this.props.elements.map((el) => {
//                         return(
//                             <EducationItem 
//                             key={`${el.title} - ${el.schoolName}`}
//                             title={el.title} schoolName={el.schoolName} description={el.description}/>
//                         )
//                     })}
//                 </div>
//             </div>
//         )
//     }
// }

const Education = (props) => {
    return (
        <div className="education">
            <div className="education__list">
                {props.elements.map((el) => {
                    return(
                        <EducationItem 
                            key={`${el.title} - ${el.schoolName}`}
                            title={el.title} schoolName={el.schoolName} description={el.description}
                        />
                    )
                })}
            </div>
        </div>
    )
}

export default Education;