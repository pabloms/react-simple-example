import React from 'react';
import { useState } from 'react';
import './NewJobForm.scss';

// const INITIAL_STATE = {
//     workstation: '',
//     companyName: '',
//     description: '',
// }

// class NewJobForm extends React.Component {

//     state = INITIAL_STATE

//     handleFormSubmit = (e) => {
//         e.preventDefault();
//         this.props.addJob(this.state);

//         this.setState(INITIAL_STATE)
//     }

//     handleChangeInput = (e) => {
//         this.setState({
//             [e.target.name]: e.target.value 
//         }) 
//     }

//     render() {
//         return (
//             <form className="form" onSubmit={this.handleFormSubmit}>

//             <label>
//                 <p>Puesto de Trabajo:</p>
//                 <input
//                     type="text"
//                     name="workstation"
//                     value={this.state.workstation}
//                     onChange={this.handleChangeInput}
//                 />
//             </label>

//             <label>
//                 <p>Nombre de la compañía:</p>
//                 <input
//                     type="text"
//                     name="companyName"
//                     value={this.state.companyName}
//                     onChange={this.handleChangeInput}
//                 />
//             </label>

//             <label>
//                 <p>Descrpción:</p>
//                 <input
//                     type="text"
//                     name="description"
//                     value={this.state.description}
//                     onChange={this.handleChangeInput}
//                 />
//             </label>

//             <div className="form__button">
//                 <input type="submit" value="Agregar nuevo trabajo!"/>
//             </div>

//         </form>

//         )
//     }
// }

const NewJobForm = (props) => {

    const [state, setState] = useState({
        workstation: '',
        companyName: '',
        description: '',
    });

    const handleFormSubmit = (e) => {
        e.preventDefault();
        props.addJob(state);

        setState({
            workstation: '',
            companyName: '',
            description: '',
        });
    };

    const handleChangeInput = (e) => {
        const {name, value} = e.target;
        setState({
            // ...state, para que no se borre lo anterior escrito en el input
            ...state,
            [name] : value,
        }) 
    };

    return (
        <form className="form" onSubmit={handleFormSubmit}>
            <label>
                <p>Puesto de Trabajo:</p>
                <input
                    type="text"
                    name="workstation"
                    value={state.workstation}
                    onChange={handleChangeInput}
                />
            </label>
            <label>
                <p>Nombre de la compañía:</p>
                <input
                    type="text"
                    name="companyName"
                    value={state.companyName}
                    onChange={handleChangeInput}
                />
            </label>
            <label>
                <p>Descrpción:</p>
                <input
                    type="text"
                    name="description"
                    value={state.description}
                    onChange={handleChangeInput}
                />
            </label>
            <div className="form__button">
                <input type="submit" value="Agregar nuevo trabajo!"/>
            </div>
        </form>
    )
}

export default NewJobForm;