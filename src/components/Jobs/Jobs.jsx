import React from 'react';
import './Jobs.scss';
import Job from '../Job';

// class Jobs extends React.Component {
//     render() {
//         return (
//             <div className="jobs">
//                 <ul className="jobs__list">
//                     {this.props.elements.map((el) => {
//                         return(
//                             <Job 
//                             key={`${el.workstation} - ${el.companyName}`}
//                             workstation={el.workstation} companyName={el.companyName} description={el.description}/>
//                         )
//                     })}
//                 </ul>
//             </div>
//         )
//     }
// }

const Jobs = (props) => {
    return (
        <div className="jobs">
            <ul className="jobs__list">
                {props.elements.map((el) => {
                    return(
                        <Job 
                            key={`${el.workstation} - ${el.companyName}`}
                            workstation={el.workstation} companyName={el.companyName} description={el.description}
                        />
                    )
                })}
            </ul>
        </div>
    )
}

export default Jobs;