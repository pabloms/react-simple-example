import React from 'react';
import './Header.scss';

// class Header extends React.Component {
//     render() {
//         return (
//             <header className="header">
//                 <img src={this.props.image} alt={this.props.fullName}/> 
//                 <div className="header__info">
//                     <h1>{this.props.fullName}</h1>
//                     <h2>{this.props.profession}</h2>
//                 </div>
//             </header>
//         )
//     }
// }

const Header = (props) => {
    return (
        <header className="header">
            <img src={props.image} alt={props.fullName}/> 
            <div className="header__info">
                <h1>{props.fullName}</h1>
                <h2>{props.profession}</h2>
            </div>
        </header>
    )
}

export default Header;