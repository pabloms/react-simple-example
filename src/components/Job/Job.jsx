import React from 'react';
import './Job.scss';

// class Job extends React.Component {
//     render() {
//         return (
//             <div className="item">
//                 <h2>{this.props.workstation}</h2>
//                 <h3>{this.props.companyName}</h3>
//                 <p>{this.props.description}</p>
//                 <hr></hr>
//             </div>
//         )
//     }
// }

const Job = (props) => {
    return (
        <div className="item">
            <h2>{props.workstation}</h2>
            <h3>{props.companyName}</h3>
            <p>{props.description}</p>
            <hr></hr>
        </div>
    )
}

export default Job;