import React from 'react';
import './EducationItem.scss';

// class EducationItem extends React.Component {
//     render() {
//         return (
//             <div className="item">
//                 <h2>{this.props.title}</h2>
//                 <h3>{this.props.schoolName}</h3>
//                 <p>{this.props.description}</p>
//                 <hr></hr>
//             </div>
//         )
//     }
// }

const EducationItem = (props) => {
    return (
        <div className="item">
            <h2>{props.title}</h2>
            <h3>{props.schoolName}</h3>
            <p>{props.description}</p>
            <hr></hr>
        </div> 
    )
}

export default EducationItem;